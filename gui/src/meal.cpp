#include <mealPlanner/meal.h>
#include <QDebug>
#include <ui/mealPlanner/ui_meal.h>
#include <QObject>
#include <QAction>

QDataStream &operator <<(QDataStream& out, const Meal &obj){
	out << obj.name;
	out << obj.time;
	out << obj.servings;
	out << obj.ingredients;
	return out;
}

QDataStream &operator >>(QDataStream& in, Meal &obj){
	in >> obj.name;
	in >> obj.time;
	in >> obj.servings;
	in >> obj.ingredients;
	return in;
}

void Meal::setTime(const QTime &value){
    time = value;
}

void Meal::setName(const QString &value){
    name = value;
}

void Meal::setServings(const qint8 &value){
    servings = value;
}


Meal::Meal(QString _name, QTime _time,
           qint8 _servings, QList<Ingredient> _ingredients,
           QListWidget *_parent, int _type) :
        QListWidgetItem(_parent, _type),
        ui(new Ui::meal_form),
        widget(new QWidget),
        action_edit_meal("Edit"){
    name = _name;
    time = _time;
    servings = _servings;
    ingredients = _ingredients;
    ui->setupUi(widget);
    ui->meal_label->setText(name);
	set_widget = false;
}

Meal::Meal(const Meal &other) :
    Meal(other.name, other.time,
         other.servings, other.ingredients){}

Meal& Meal::operator =(const Meal &other){
    name = other.name;
    time = other.time;
    servings = other.servings;
    ingredients = other.ingredients;
    ui->meal_label->setText(name);
    return *this;
}

void Meal::add_ingredient(Ingredient ingredient){
	//qDebug() << "Adding ingredient:" << ingredient.get_name();
    ingredients.append(ingredient);
}

QList<Ingredient> Meal::get_ingredients(){
    return ingredients;
}

QWidget *Meal::get_widget() const{
    return widget;
}

QString Meal::get_name() const{
    return name;
}

QTime Meal::get_time() const{
    return time;
}

qint8 Meal::get_servings() const{
	return servings;
}

void Meal::clear_ingredients(){
    ingredients.clear();
}

void Meal::add_to_list_widget(QListWidget *list_widget, bool set_widget){
    setSizeHint(QSize(sizeHint().width(), 30));
    setText(name);
    list_widget->addItem(this);
	this->set_widget = set_widget;
    if (!set_widget) return;
    setText(QString());
    list_widget->setItemWidget(this, widget);
    setSizeHint(QSize(sizeHint().width(), 80));
    return;
}

void Meal::update_ui(){
	ui->meal_label->setText(name);
	if (!set_widget)
		setText(name);
}


void Meal::on_meal_edit_triggered(bool checked){
	Q_UNUSED(checked)
	qDebug() << "Edit triggered";
}

void Meal::to_json(QJsonObject &json) const{
	json["name"] = name;
	json["servings"] = servings;
	QJsonObject time_object;
	time_object["hours"] = time.hour();
	time_object["minutes"] = time.minute();
	json["time"] = time_object;
}
