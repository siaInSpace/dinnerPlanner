#include <mealPlanner/ingredient.h>
#include <QDebug>


QDataStream &operator <<(QDataStream& out, const Ingredient &obj){
	out << obj.name;
	out << obj.amount;
	out << obj.unit;
	return out;
}

QDataStream &operator >>(QDataStream& in, Ingredient &obj){
	in >> obj.name;
	in >> obj.amount;
	in >> obj.unit;
	return in;
}

Ingredient::Ingredient(QString _name, qint16 _amount,
		QString _unit, QListWidget *_parent, int _type) :
		QListWidgetItem(_parent, _type),
		ui(new Ui::ingredient_form),
		widget(new QWidget){
    name = _name;
    amount = _amount;
	unit = _unit;
	ui->setupUi(widget);
}

Ingredient::Ingredient(const Ingredient &other) :
    Ingredient(other.name, other.amount, other.unit){
}

Ingredient& Ingredient::operator =(const Ingredient &other){
    if (this == &other) return *this; //Is same object
    name = other.name;
    amount = other.amount;
    unit = other.unit;
    return *this;
}

QString Ingredient::get_name() const{
    return name;
}

QString Ingredient::get_unit() const{
    return  unit;
}

qint16 Ingredient::get_amount() const{
    return  amount;
}


//Checks if ingredients are the same based on the name and unit
//Amount and units are not checked
bool Ingredient::operator ==(const Ingredient &other) const{
    if (this == &other) return true; //return true if same object
	if (name != other.name) return false;
	if (unit != other.unit) return false;
	return true;
}

void Ingredient::add_to_list_widget(QListWidget *list_widget, bool set_widget){
	//qDebug() << name;
	setSizeHint(QSize(sizeHint().width(), 30));
	setText(name);
	list_widget->addItem(this);
	if (!set_widget) return;
	setText(QString());
	list_widget->setItemWidget(this, widget);
	setSizeHint(QSize(sizeHint().width(), 80));
	ui->name_edit->setText(name);
	ui->amount_edit->setValue(amount);
	ui->unit_edit->setText(unit);
	return;
}

Ingredient Ingredient::create_from_ui() const{
	Ingredient temp(ui->name_edit->text(), ui->amount_edit->value(), ui->unit_edit->text());
	return temp;
}
