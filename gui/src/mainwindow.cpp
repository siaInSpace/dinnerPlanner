#include <mealPlanner/mainwindow.h>
#include <ui/mealPlanner/ui_mainwindow.h>
#include <mealPlanner/meal.h>
#include <mealPlanner/newmeal.h>
#include <QDebug>
#include <QAction>
#include <QMenu>
#include <QMessageBox>
#include <QRandomGenerator>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow){
    ui->setupUi(this);
    Meal *m = new Meal("test", QTime(1,30), 4);
    Meal *m2 = new Meal("test2", QTime(0,30), 6);

	Ingredient i("Test1", 4, "dl");
	Ingredient i2("Ingredient2", 5, "kg");
	Ingredient i3("ingredient3", 500, "g");

	m->add_ingredient(i);
	m->add_ingredient(i3);

	m2->add_ingredient(i3);
	m2->add_ingredient(i2);

    m->add_to_list_widget(ui->avaliable_meals_widget);
    m2->add_to_list_widget(ui->avaliable_meals_widget);
    //Add selected with button
    QObject::connect(ui->select_meal_btn, SIGNAL(clicked(bool)), this, SLOT(add_to_selected()));
    QObject::connect(this, SIGNAL(meal_selected_with_button(QListWidgetItem*)), this, SLOT(add_to_selected(QListWidgetItem*)));
    //Add selected with double clock
    QObject::connect(ui->avaliable_meals_widget, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(add_to_selected(QListWidgetItem*)));
    QObject::connect(ui->actionAdd_Meal, SIGNAL(triggered(bool)), this, SLOT(new_meal()));
    //Right click on meal
    QObject::connect(ui->avaliable_meals_widget, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showContextMenu(QPoint)));
    QObject::connect(ui->selected_meals_widget, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showContextMenu(QPoint)));
}

MainWindow::~MainWindow(){
    delete ui;
}


bool MainWindow::confirm_remove(QString action_name){
    QMessageBox msgBox;
    msgBox.setText("Are you sure you want to remove " + action_name);
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::Yes);
    return msgBox.exec() == QMessageBox::Yes;
}

//Acts based on action selected with right click menu
void MainWindow::act_on_context_menu(rightClickResult result, QListWidget *sent_from){
    switch (result) {
    case rightClickResult::Edit:
        new_meal((Meal *)sent_from->currentItem());
        break;
    case rightClickResult::Delete:
        if (!confirm_remove(((Meal*)sent_from->currentItem())->get_name())) break;
        delete sent_from->takeItem(sent_from->currentRow());
        break;
    default:
        break;
    }
}

//Slots
// Reemit signal with current item
void MainWindow::add_to_selected(){
    emit meal_selected_with_button(ui->avaliable_meals_widget->currentItem());
}

//Adds meal to selected meals
void MainWindow::add_to_selected(QListWidgetItem* selected){
    Meal *meal_to_add = new Meal(*((Meal*)selected));
    meal_to_add->add_to_list_widget(ui->selected_meals_widget, true);
}

//Remove selected meal(s) from selected meals widget
void MainWindow::on_deselect_meal_btn_clicked(){
    qDeleteAll(ui->selected_meals_widget->selectedItems());
}

void MainWindow::new_meal(){
	new_meal(nullptr);
}

//Shows new window to populate new meal
//If the meal is being created add it to the list else meal will be updated
void MainWindow::new_meal(Meal *meal){
	bool add_new_meal = false;
	if(!meal){
		meal = new Meal();
		add_new_meal = true;
		qDebug() << "No meal!";
	}
    NewMeal *newMeal = new NewMeal(meal);
    int result = newMeal->exec();
    switch (result) {
    case QDialog::Accepted:
		//qDebug() << "New meal accepted!";
		if(add_new_meal)
			meal->add_to_list_widget(ui->selected_meals_widget);
		meal->update_ui();
        break;
    case QDialog::Rejected:
		//qDebug() << "New meal rejected";
		if(add_new_meal)
			delete(meal);
        break;
    default:
		qDebug() << "Unkown exit code: " << result;
        break;
    }
}

//Configures right click menu for avaliable meals
void MainWindow::showContextMenu(const QPoint &pos){
    //QObject *obj = sender();
    //QListWidget *sent_from = nullptr;
    QListWidget *sent_from = qobject_cast<QListWidget *>(sender());
    if(!sent_from) return;
    rightClickResult result;
    QMenu submenu;
    submenu.addAction("Edit");
    submenu.addAction("Remove");
    QAction *selected = submenu.exec(sent_from->mapToGlobal(pos));
    if(!selected) return;//Nothing selected
    if(selected->text().contains("Edit")) result = rightClickResult::Edit;
    if(selected->text().contains("Remove")) result = rightClickResult::Delete;
    qDebug() << result;
    act_on_context_menu(result, sent_from);
}

void MainWindow::on_add_random_ingredient_btn_clicked(){
	quint32 rand = QRandomGenerator::global()->bounded(ui->avaliable_meals_widget->count());
	//ui->avaliable_meals_widget->setCurrentRow(rand);
	if (!ui->allow_duplicants_chkbox->isChecked())
		emit meal_selected_with_button(ui->avaliable_meals_widget->item(rand));
	//Check if already selected


	qDebug() << "Add random meal" << rand;
}

void MainWindow::on_actionSave_plan_triggered(){
	qDebug() << "Save meals!";
/*
	QFile f("test.txt");
	if (!f.open(QIODevice::WriteOnly | QIODevice::Text))
		return;
	QDataStream out(&f);
	for (int i = 0; i < ui->selected_meals_widget->count(); i++){
		Meal *m = (Meal*)ui->selected_meals_widget->item(i);
		out << *m;
	}
	f.close();
*/
	QFile f("test.json");
	if (!f.open(QIODevice::WriteOnly | QIODevice::Text))
		return;

	QJsonObject json_object;
	QJsonDocument json_doc;
	QJsonArray json_meals;
	for (int i = 0; i < ui->selected_meals_widget->count(); ++i) {
		QJsonObject meal_object;
		((Meal*)ui->selected_meals_widget->item(i))->to_json(meal_object);
		json_meals.append(meal_object);
	}
	f.write(QJsonDocument(json_meals).toJson());
}










