#include <mealPlanner/newmeal.h>
#include <QDebug>

NewMeal::NewMeal(Meal *_meal, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewMeal){
    ui->setupUi(this);
    if (_meal){
        ui->name_edit->setText(_meal->get_name());
        ui->time_edit->setTime(_meal->get_time());
		ui->servings_edit->setValue(_meal->get_servings());
        ui->ingredients_list->clear();
		QList<Ingredient> ingredients = _meal->get_ingredients();
		for (auto i = ingredients.begin(); i != ingredients.end(); i++){
			Ingredient *ing = new Ingredient(*i);
			ing->add_to_list_widget(ui->ingredients_list, true);
		}
        meal = _meal;
    }else{
        meal = new Meal();
    }
}

NewMeal::~NewMeal(){
	ui->ingredients_list->clear();
    delete ui;
}

void NewMeal::on_NewMeal_accepted(){
    meal->setName(ui->name_edit->text());
    meal->setTime(ui->time_edit->time());
	meal->setServings(ui->servings_edit->value());
    meal->clear_ingredients();
    for (int i = 0; i < ui->ingredients_list->count(); i++){
		meal->add_ingredient(((Ingredient *)ui->ingredients_list->item(i))->create_from_ui());
    }
}

