#include <mealPlanner/mylistwidget.h>
#include <mealPlanner/meal.h>
#include <QDropEvent>
#include <QDebug>
#include <QMimeData>
#include <QStandardItemModel>
MyListWidget::MyListWidget(QWidget *parent) : QListWidget(parent){
}


void MyListWidget::dropEvent(QDropEvent *event){
    if (event->source() == this){
        event->setDropAction(Qt::DropAction::MoveAction);
    }else{
        event->setDropAction(Qt::DropAction::CopyAction);
    }
    event->accept();
    qDebug() << event;
    /*
    qDebug() << event->mimeData()->formats();
    */
    /*
    QStandardItemModel model;
    model.dropMimeData(event->mimeData(), Qt::CopyAction, 0, 0, QModelIndex());
    QListWidgetItem *item = ((QListWidgetItem*)(model.item(0, 0)));
    ;
    //qDebug() << ((Meal*)item)->get_name();
    */
    QByteArray encodeed = event->mimeData()->data("application/x-qabstractitemmodeldatalist");
    QDataStream stream(&encodeed, QIODevice::ReadOnly);
    while(!stream.atEnd()){
        int row, col;
        QMap<int, QVariant> dataMap;
        stream >> row >> col >> dataMap;
        qDebug() << dataMap;
    }
}

