#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include <mealPlanner/meal.h>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    bool confirm_remove(QString action_name);
    enum rightClickResult {Edit, Delete};
    void act_on_context_menu(rightClickResult result, QListWidget *sent_from);
private slots:
    void add_to_selected();
    void add_to_selected(QListWidgetItem *selected);
	void new_meal();
	void new_meal(Meal *meal);
    void showContextMenu(const QPoint &pos);
    void on_deselect_meal_btn_clicked();
	void on_add_random_ingredient_btn_clicked();

	void on_actionSave_plan_triggered();

signals:
    void meal_selected_with_button(QListWidgetItem *selected);
};

#endif // MAINWINDOW_H
