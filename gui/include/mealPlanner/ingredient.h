#ifndef INGREDIENT_H
#define INGREDIENT_H
#include <QString>
#include <QListWidgetItem>
#include <ui/mealPlanner/ui_ingredient.h>

class Ingredient : public QListWidgetItem{
private:
    QString name;
    qint16 amount;
    QString unit;
	Ui::ingredient_form *ui;
	QWidget *widget;
public:
	Ingredient(QString name = "", qint16 amount = 0, QString unit = "",
			   QListWidget *parent = 0, int type = Type);
    Ingredient(const Ingredient &other);
    Ingredient &operator =(const Ingredient &other);
	Ingredient create_from_ui() const;
    bool operator ==(const Ingredient &other) const;
    QString get_name() const;
    qint16 get_amount() const;
    QString get_unit() const;
	QWidget *get_widget() const;
	void add_to_list_widget(QListWidget *list_widget, bool set_widget = false);
	friend QDataStream &operator <<(QDataStream& out, const Ingredient &obj);
	friend QDataStream &operator >>(QDataStream& in, Ingredient &obj);
};

#endif // INGREDIENT_H
