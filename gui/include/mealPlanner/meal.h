#ifndef MEAL_H
#define MEAL_H
#include <QObject>
#include <QString>
#include <QTime>
#include <QList>
#include <mealPlanner/ingredient.h>
#include <QListWidgetItem>
#include <QWidget>
#include <QMainWindow>
#include <QAction>
#include <ui/mealPlanner/ui_meal.h>
#include <QJsonObject>
#include <QJsonArray>

class Meal : public QListWidgetItem{
private:
    QString name;
    QTime time;
    qint8 servings;
    QList<Ingredient> ingredients;
    Ui::meal_form *ui;
    QWidget *widget;
	bool set_widget;
public:
    Meal(QString name = "", QTime time = QTime(0, 0),
		 qint8 servings = 4,
         QList<Ingredient> ingredients = QList<Ingredient>(),
         QListWidget *parent = 0, int type = Type);
    Meal(const Meal &other);
    Meal& operator =(const Meal &other);
    void add_ingredient(Ingredient ingredient);
	QList<Ingredient> get_ingredients();
    QWidget *get_widget() const;
    QString get_name() const;
    QTime get_time() const;
	qint8 get_servings() const;
    void add_to_list_widget(QListWidget *list_widget, bool set_widget=false);
    QAction action_edit_meal;
    void setTime(const QTime &value);
    void setName(const QString &value);
    void setServings(const qint8 &value);
    void clear_ingredients();
	void update_ui();
	void to_json(QJsonObject &json) const;
	friend QDataStream &operator <<(QDataStream& out, const Meal &obj);
	friend QDataStream &operator >>(QDataStream& in, Meal &obj);

private slots:
    void on_meal_edit_triggered(bool checked = false);
};

#endif // MEAL_H
