#ifndef NEWMEAL_H
#define NEWMEAL_H
#include <ui/mealPlanner/ui_newmeal.h>
#include <QDialog>
#include <mealPlanner/meal.h>

class NewMeal : public QDialog
{
    Q_OBJECT

public:
    NewMeal(Meal *meal = 0, QWidget *parent = 0);
    ~NewMeal();
    Meal *get_meal();

private slots:
    void on_NewMeal_accepted();

private:
    Ui::NewMeal *ui;
    Meal *meal;
};

#endif // NEWMEAL_H
