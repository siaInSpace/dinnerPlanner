#ifndef MYLISTWIDGET_H
#define MYLISTWIDGET_H
#include <QListWidget>
class MyListWidget : public QListWidget{
    Q_OBJECT
public:
    MyListWidget(QWidget *parent = nullptr);
    void dropEvent(QDropEvent *event);
};
#endif
