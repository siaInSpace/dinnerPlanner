SOURCES += ../gui/src/testmethos.cpp \
    ../gui/src/meal.cpp \
    ../gui/src/ingredient.cpp \
    ../gui/src/mainwindow.cpp \
    ../gui/src/newmeal.cpp \
    ../gui/src/mylistwidget.cpp


HEADERS += ../gui/include/mealPlanner/testmethos.h \
    ../gui/include/mealPlanner/meal.h \
    ../gui/include/mealPlanner/ingredient.h \
    ../gui/include/mealPlanner/mainwindow.h \
    ../gui/include/mealPlanner/newmeal.h \
    ../gui/include/mealPlanner/mylistwidget.h

FORMS += ../gui/forms/meal.ui \
    $$PWD/forms/ingredient.ui \
    ../gui/forms/mainwindow.ui \
    ../gui/forms/newmeal.ui
