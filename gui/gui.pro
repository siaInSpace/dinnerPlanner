#-------------------------------------------------
#
# Project created by QtCreator 2020-05-26T19:25:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mealPlanner
TEMPLATE = app
DESTDIR = bin/
INCLUDEPATH = include/
UI_DIR = ui/mealPlanner/
MOC_DIR = moc/
OBJECTS_DIR = obj/

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += ../gui/src/main.cpp
include(../gui/gui_sources.pri)




