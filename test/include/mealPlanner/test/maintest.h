#ifndef MAINTEST_H
#define MAINTEST_H
#include <QString>
#include <QtTest>
#include <mealPlanner/testmethos.h>
#include <mealPlanner/meal.h>
#include <mealPlanner/ingredient.h>
#include <mealPlanner/mainwindow.h>

class MainTest : public QObject
{
    Q_OBJECT

public:
    MainTest();

private slots:
    void meal_add_ingredients();
};

#endif // MAINTEST_H
