#include <mealPlanner/test/maintest.h>

MainTest::MainTest(){
}


void MainTest::meal_add_ingredients(){
    Meal m1("Meal1");
    Ingredient i1("Sugar", 100, "g");
    Ingredient i2("Wheat", 1, "kg");
    Ingredient i3("yeast", 25, "g");
    Ingredient i4("water", 1, "L");
    m1.add_ingredient(i1);
    m1.add_ingredient(i2);
    m1.add_ingredient(i3);
    m1.add_ingredient(i4);
    QList<Ingredient> ingredients;
    ingredients.append(i1);
    ingredients.append(i2);
    ingredients.append(i3);
    ingredients.append(i4);
    QCOMPARE(m1.get_ingredients(), ingredients);
    Meal m2("meal2", QTime(0, 30), 4, ingredients=ingredients);
    QCOMPARE(m1.get_ingredients(), m2.get_ingredients());
}

