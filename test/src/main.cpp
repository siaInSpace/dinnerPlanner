#include <mealPlanner/test/maintest.h>
#include <QTest>

int main(int argc, char **argv){
    int status = 0;
    qDebug() << "Testing";
    QApplication app(argc, argv);
    {
        MainTest mainTest;
        status |= QTest::qExec(&mainTest, argc, argv);
    }

    return status;
}
