QT       += testlib core gui widgets

QT       -= gui

TARGET = test
CONFIG   += console
CONFIG   -= app_bundle
DESTDIR = bin/
INCLUDEPATH += ../test/include/
MOC_DIR = moc/
OBJECTS_DIR = obj/
UI_DIR = ui/mealPlanner/

TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += $$files(../test/src/*.cpp)
HEADERS += $$files(../test/include/mealPlanner/test/*.h)


include(../gui/gui_sources.pri)
INCLUDEPATH += ../gui/include

